package be.dynamix.rampup.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(name = "product")
@NoArgsConstructor
@Getter
@Setter

public class Product extends BaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2197460881360423586L;

	
	@Column(name = "product_ID", length = 250, nullable = false)
	private String productId;
	
	@Column(name = "name", length = 50)
	private String name;
	
	@Column(name = "price", length = 50)
	private Double price;
	
	

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumn(name = "command_id")
	private Command command;
}
