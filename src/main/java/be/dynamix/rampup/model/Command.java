package be.dynamix.rampup.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "command")
@Getter
@Setter
public class Command extends BaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6018007469464834924L;

	
	@Column(name = "command_ID", length = 250, nullable = false)
	private String commandId;
	
	
	@Column(name = "title", length = 250)
	private String title;

	@Column(name = "description", length = 250)
	private String description;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "command")
	private Set<Product> products = new HashSet<>(0);

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumn(name = "client_id")
	private Client client;

}
