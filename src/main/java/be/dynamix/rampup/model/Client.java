package be.dynamix.rampup.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "client")
@Getter
@Setter
// @XmlRootElement(name = "Client")
public class Client extends BaseEntity implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8233958664034906480L;

	@Column(name = "client_ID", length = 250, nullable = false)
	private String clientId;

	@Column(name = "name", length = 250)
	private String name;

	@Column(name = "surname", length = 250)
	private String surname;

	@Column(name = "email", length = 50)
	private String email;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "client")
	@JsonIgnore
	private Set<Command> commands = new HashSet<>(0);

}
