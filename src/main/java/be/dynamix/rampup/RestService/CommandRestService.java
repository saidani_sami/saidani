package be.dynamix.rampup.RestService;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import be.dynamix.rampup.model.Command;
import be.dynamix.rampup.service.CommandService;

@Component
@Produces(MediaType.APPLICATION_JSON)
public class CommandRestService {

	@Autowired
	private CommandService commandService;
	
	@GET
	@Path("/{id}/commands")
	public List<Command> getCommandsByClientId(@PathParam("id") Long id) {
		List<Command> list = commandService.findCommandByClientId(id);
		return list;
	}
	
	@GET
	@Path("/{clientId}/commands/{commandId}")
	public Command findByCommandIdAndClientId(@PathParam("commandId") Long commandId,@PathParam("clientId") Long clientId) {
		Command c = commandService.findByCommandIdAndClientId(commandId, clientId);
		return c;
	}

}
