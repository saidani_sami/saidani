package be.dynamix.rampup.RestService;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import be.dynamix.rampup.model.Client;
import be.dynamix.rampup.service.ClientService;

@Component
@Path("/clientservice")
@Produces(MediaType.APPLICATION_JSON)
public class ClientRestService {

	@Autowired
	private ClientService clientService;
	
	@GET
	@Path("/details")
	public List<Client> getClients() {
		List<Client> list = clientService.findAll();
		return list;
	}
	
	@GET
	@Path("/clients/{id}")
	public Client getArticleById(@PathParam("id") Long id) {
		Client client = clientService.findOne(id);
		return client;
	}
}
