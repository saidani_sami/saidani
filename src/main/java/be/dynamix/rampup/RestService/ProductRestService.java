package be.dynamix.rampup.RestService;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import be.dynamix.rampup.model.Client;
import be.dynamix.rampup.model.Product;
import be.dynamix.rampup.service.ClientService;
import be.dynamix.rampup.service.ProductService;

@Component
@Produces(MediaType.APPLICATION_JSON)
public class ProductRestService {

	@Autowired
	private ProductService productService;
	
	@GET
	@Path("/products")
	public List<Product> getProducts() {
		List<Product> list = productService.findAll();
		return list;
	}
	
	@GET
	@Path("/products/{id}")
	public Product getProductsById(@PathParam("id") Long id) {
		Product p = productService.findOne(id);
		return p;
	}
}
