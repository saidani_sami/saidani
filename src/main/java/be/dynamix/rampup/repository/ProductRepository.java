package be.dynamix.rampup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import be.dynamix.rampup.model.Product;

/**
 * 
 * @ Saidani Sami
 *
 */

public interface ProductRepository extends JpaRepository<Product, Long>, JpaSpecificationExecutor<Product> {

	Product findByProductId(String productId);

	Product findByName(String name);

}
