package be.dynamix.rampup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import be.dynamix.rampup.model.Client;

/**
 * 
 * @ Saidani Sami
 *
 */

public interface ClientRepository extends JpaRepository<Client, Long>, JpaSpecificationExecutor<Client> {

	Client findByClientId(String clientId);
	
	Client findByName(String name);
	
	

}

