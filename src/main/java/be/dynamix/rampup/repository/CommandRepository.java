package be.dynamix.rampup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import be.dynamix.rampup.model.Command;

/**
 * 
 * @ Saidani Sami
 *
 */

public interface CommandRepository extends JpaRepository<Command, Long>, JpaSpecificationExecutor<Command> {

	Command findByCommandId(String commandId);

	Command findByTitle(String title);

	@Query("select c from Command c where c.client.id=?1 ")
	List<Command> findCommandByClientId(Long clientId);

	@Query("select c from Command c  where c.id=?1 and c.client.id=?2")
	Command findByCommandIdAndClientId(Long commandId, Long clientId);

}
