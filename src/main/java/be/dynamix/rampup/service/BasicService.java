package be.dynamix.rampup.service;

import java.util.List;


/**
 * 
 * @author Saidani Sami
 *
 */

public interface BasicService<T> {

	public T save(T t) ;

	public List<T> findAll();

	public void update(T t);

	public void delete(T t);

	public T findOne(Long id);

}
