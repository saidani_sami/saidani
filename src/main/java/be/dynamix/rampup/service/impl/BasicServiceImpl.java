package be.dynamix.rampup.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;

import be.dynamix.rampup.service.BasicService;

public abstract class BasicServiceImpl<T, X extends JpaRepository<T, Long>> implements BasicService<T> {
	@Autowired
	public X repository;

	@Override
	public T save(T t) {
		return repository.save(t);
	}

	@Override
	public List<T> findAll() {
		return repository.findAll();
	}

	@Override
	public void update(T t) {

	}

	@Override
	public void delete(T t) {
		repository.delete(t);
	}

	@Override
	public T findOne(Long id) {
		return repository.findOne(id);

	}
}
