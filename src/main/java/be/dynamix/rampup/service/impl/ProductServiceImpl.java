package be.dynamix.rampup.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import be.dynamix.rampup.model.Product;
import be.dynamix.rampup.repository.ProductRepository;
import be.dynamix.rampup.service.ProductService;
@Service
@Transactional(rollbackFor = Exception.class)
public class ProductServiceImpl extends BasicServiceImpl<Product, ProductRepository> implements ProductService {

	@Override
	public Product findByProductId(String productId) {
		return repository.findByProductId(productId);
	}

}
