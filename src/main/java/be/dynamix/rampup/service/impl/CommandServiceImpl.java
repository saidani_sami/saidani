package be.dynamix.rampup.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import be.dynamix.rampup.model.Command;
import be.dynamix.rampup.repository.CommandRepository;
import be.dynamix.rampup.service.CommandService;
@Service
@Transactional(rollbackFor = Exception.class)
public class CommandServiceImpl extends BasicServiceImpl<Command, CommandRepository> implements CommandService {

	@Override
	public Command findByCommandId(String commandId) {
		return repository.findByCommandId(commandId);
	}

	@Override
	public List<Command> findCommandByClientId(Long clientId) {
		return repository.findCommandByClientId(clientId);
	}

	@Override
	public Command findByCommandIdAndClientId(Long commandId, Long clientId) {
		return repository.findByCommandIdAndClientId(commandId, clientId);
	}

}
