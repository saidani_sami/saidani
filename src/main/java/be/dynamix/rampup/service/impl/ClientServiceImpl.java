package be.dynamix.rampup.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import be.dynamix.rampup.model.Client;
import be.dynamix.rampup.repository.ClientRepository;
import be.dynamix.rampup.service.ClientService;

@Service
@Transactional(rollbackFor = Exception.class)
public class ClientServiceImpl extends BasicServiceImpl<Client, ClientRepository> implements ClientService {

	@Override
	public Client findByClientId(String clientId) {
		return repository.findByClientId(clientId);
	}

}
