package be.dynamix.rampup.service;

import java.util.List;

import be.dynamix.rampup.model.Command;

public interface CommandService extends BasicService<Command>{

	public Command findByCommandId(String commandId);
	
	
	public List<Command> findCommandByClientId(Long clientId);
	
	
	public Command findByCommandIdAndClientId(Long commandId, Long clientId);
}
