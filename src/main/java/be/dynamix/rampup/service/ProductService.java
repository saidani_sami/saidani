package be.dynamix.rampup.service;

import be.dynamix.rampup.model.Product;

public interface ProductService extends BasicService<Product>{

	public Product findByProductId(String productId);
	
	
	
}
