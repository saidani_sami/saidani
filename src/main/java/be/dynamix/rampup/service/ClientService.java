package be.dynamix.rampup.service;

import be.dynamix.rampup.model.Client;


public interface ClientService extends BasicService<Client>{

	public Client findByClientId(String clientId);
	
	
	
}
