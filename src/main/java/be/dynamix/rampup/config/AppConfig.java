package be.dynamix.rampup.config;

import java.util.ArrayList;
import java.util.List;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import be.dynamix.rampup.RestService.ClientRestService;
import be.dynamix.rampup.RestService.CommandRestService;
import be.dynamix.rampup.RestService.ProductRestService;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = { "be.dynamix.rampup.**" })

public class AppConfig {

	@Autowired
	private ClientRestService clientRestService;

	@Autowired
	private CommandRestService commandRestService;

	@Autowired
	private ProductRestService productRestService;

	@Bean(destroyMethod = "shutdown")
	public SpringBus cxf() {
		return new SpringBus();
	}

	@Bean(destroyMethod = "destroy")
	@DependsOn("cxf")
	public Server jaxRsServer() {
		final JAXRSServerFactoryBean factory = new JAXRSServerFactoryBean();
		List<Object> services = new ArrayList<>();
		services.add(clientRestService);
		services.add(commandRestService);
		services.add(productRestService);
		factory.setServiceBeans(services);

		factory.setProvider(new JacksonJsonProvider());
		factory.setBus(cxf());
		factory.setAddress("/");
		return factory.create();
	}

	@Bean
	public ServletRegistrationBean cxfServlet() {
		final ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(new CXFServlet(), "/api/*");
		servletRegistrationBean.setLoadOnStartup(1);
		return servletRegistrationBean;
	}
}
