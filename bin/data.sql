insert into client (client_ID,name,surname) values('X562Kk','saidani','sami');
insert into client (client_ID,name,surname) values('Z569RT','James','Bond');
insert into client (client_ID,name,surname) values('K986IO','Albert','Einshtein');
insert into client (client_ID,name,surname) values('T547PL','Adolphe','Hitler');

insert into command (command_ID,title,description,client_ID) values ('X1','Command 1', 'Command for xxxx',1);
insert into command (command_ID,title,description,client_ID) values ('X2','Command 2', 'Command for xxxx',2);
insert into command (command_ID,title,description,client_ID) values ('X3','Command 3', 'Command for xxxx',3);
insert into command (command_ID,title,description,client_ID) values ('X4','Command 4', 'Command for xxxx',4);

insert into product (product_ID, name, price,command_ID ) values('10','Memory Card',10.5,1);
insert into product (product_ID, name, price,command_ID) values('20','PC',750.5,1);
insert into product (product_ID, name, price,command_ID) values('30','Phone',300.0,2);
insert into product (product_ID, name, price,command_ID) values('40','Mac',400.5,2);
insert into product (product_ID, name, price,command_ID) values('50','Battery',50.5,3);
insert into product (product_ID, name, price,command_ID) values('60','Recorder',50.5,4);



